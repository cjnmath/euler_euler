

def sum_of_multiples(num1, num2, below):
    if num1 > num2:
        num1, num2 = num2, num1
    multiples1 = [num1*i for i in range(below) if num1*i <= below]
    multiples2 = [num2*i for i in range(below) if num2*i <= below]
    multiples1.extend(multiples2)
    sum = 0
    for num in set(multiples1):
        sum += num
    return sum


if __name__ == '__main__':
    s = sum_of_multiples(3, 5, 1000)
    print(s)
