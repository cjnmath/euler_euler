def generate_Fibonacci_list(not_exceed):
    if not_exceed <= 0:
        return None
    if not_exceed == 1:
        return [1]
    if not_exceed == 2:
        return [1, 2]

    l = [1, 2]
    while True:
        n = l[-1] + l[-2]
        if n <= not_exceed:
            l.append(n)
        else:
            break
    return l


def Even_Fibonacci_numbers(not_exceed):
    sum = 0
    targets = generate_Fibonacci_list(not_exceed=not_exceed)
    for i in targets:
        if i % 2 == 0:
            sum += i
    return sum


if __name__ == '__main__':
    r = Even_Fibonacci_numbers(4000000)
    print(r)
