

def is_multiple(num, num_list):
    for i in num_list:
        if num % i == 0:
            return True
    return False


def max_pow(num, factor):
    """ return power, quotient
    """
    remaider = num % factor
    if remaider != 0:
        return 0, num
    power = 1
    num = num / factor
    while num % factor == 0:
        power += 1
        num = num / factor
    return power, int(num)


def factor(num):
    primes = [2]
    test_num = 2
    num_factors = {}
    while True:
        if not is_multiple(test_num, primes) or test_num == 2:
            primes.append(test_num)
            if num % test_num == 0:
                power, num = max_pow(num, test_num)
                num_factors[test_num] = power
        test_num += 1
        if num == 1:
            break
    return num_factors


def Largest_prime_factor(num):
    factors = factor(num)
    return max(factors.keys())


if __name__ == '__main__':
    r = Largest_prime_factor(600851475143)
    print(r)    # r = 6857
