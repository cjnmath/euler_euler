def is_palindromic(num):
    s = str(num)
    length = len(s)
    for i in range(length):
        if s[i] != s[length-1-i]:
            return False
    return True


def Largest_palindrome_product(digit):
    min_num = 10 ** (digit - 1)
    max_num = 10 ** digit - 1
    num_range = [i for i in range(min_num, max_num + 1)]
    num_range.reverse()
    for i in num_range:
        find = False
        for j in num_range:
            if is_palindromic(i*j):
                print(i, '*', j, '=', i*j)
                find = True
                break
        if find:
            break


if __name__ == '__main__':
    Largest_palindrome_product(3)
