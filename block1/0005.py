def is_multiple(num, num_list):
    for i in num_list:
        if num % i == 0:
            return True
    return False


def max_pow(num, factor):
    """ return power, quotient
    """
    remaider = num % factor
    if remaider != 0:
        return 0, num
    power = 1
    num = num / factor
    while num % factor == 0:
        power += 1
        num = num / factor
    return power, int(num)


def factor(num):
    """ num_factors prime power
    """
    primes = [2]
    test_num = 2
    num_factors = {}
    while True:
        if not is_multiple(test_num, primes) or test_num == 2:
            primes.append(test_num)
            if num % test_num == 0:
                power, num = max_pow(num, test_num)
                num_factors[test_num] = power
        test_num += 1
        if num == 1:
            break
    return num_factors


def common_multiples(num1, num2):
    factor1 = factor(num1)
    factor2 = factor(num2)
    primes = set(factor1.keys()).union(set(factor2.keys()))
    multiple = 1
    for prime in primes:
        power1 = factor1.get(prime, 1)
        power2 = factor2.get(prime, 1)
        power = max(power1, power2)
        multiple *= prime ** power
    return multiple


def Smallest_multiple(num_list):
    if len(num_list) == 1:
        return num_list[0]
    else:
        p = num_list.pop()
        return common_multiples(Smallest_multiple(num_list), p)


if __name__ == '__main__':
    l = [i for i in range(1, 21)]
    r = Smallest_multiple(l)
    print(r)
    # r = 232792560
