def Sum_square_difference(num_list):
    sum_squares = sum([i**2 for i in num_list])
    square_sum = (sum([i for i in num_list])) ** 2
    return abs(sum_squares - square_sum)


if __name__ == '__main__':
    r = Sum_square_difference([i for i in range(1, 101)])
    print(r)
