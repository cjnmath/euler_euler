def is_multiple(num, num_list):
    for i in num_list:
        if num % i == 0:
            return True
    return False


def th_prime(num):
    primes = [2]
    if num == 1:
        return 2
    primes_amount = 1
    test_prime = 3
    while primes_amount < num:
        if not is_multiple(test_prime, primes):
            primes.append(test_prime)
            primes_amount += 1
        test_prime += 1
    return primes[-1]


if __name__ == '__main__':
    r = th_prime(10001)
    print(r)
    # r = 104743
