def load_num():
    f = open('Problem_8.txt')
    lines = f.readlines()
    str_num = ''
    while lines:
        line = lines.pop(0).strip()
        str_num = str_num + line
    return str_num


def Largest_product_in_a_series(digits_amount):
    greatest_product = 0
    str_num = load_num()
    for i in range(len(str_num)-digits_amount):
        pick = [str_num[i+j] for j in range(digits_amount)]
        product = 1
        for num in map(int, pick):
            product *= num
        if greatest_product < product:
            greatest_product = product

    return greatest_product


if __name__ == '__main__':
    r = Largest_product_in_a_series(13)
    print(r)
