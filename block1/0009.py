def Special_Pythagorean_triplet(the_sum):
    for a in range(1, int(the_sum/3)+1):
        for c in range(int(the_sum/3)+1, the_sum+1):
            b = the_sum - a - c
            if a < b and b < c and a**2 + b**2 == c**2:
                print(a, b, c)
                return a*b*c


if __name__ == '__main__':
    r = Special_Pythagorean_triplet(1000)
    print(r)
