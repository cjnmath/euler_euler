
def remove_multiples(num, l):
    multiple = num
    while l and multiple <= l[-1]:
        if multiple in l:
            l.remove(multiple)
        multiple += num
    return l


def generate_prime_list(not_exceed):
    nums = [i for i in range(2, not_exceed+1)]
    primes = []
    while nums:
        prime = nums[0]
        primes.append(prime)
        nums = remove_multiples(prime, nums).copy()
    return primes


def Summation_of_primes(num):
    return sum(generate_prime_list(num))


if __name__ == '__main__':
    r = Summation_of_primes(2000000)
    print(r)
