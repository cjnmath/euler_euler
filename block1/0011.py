def load_num():
    f = open('Problem_11.txt')
    lines = f.readlines()
    matrix = []
    while lines:
        line = lines.pop(0).strip()
        m_line = map(int, line.split(' '))
        matrix.append([i for i in m_line])
    return matrix


def build_all_direction_list(m):
    result = [row for row in m]

    columns = []
    for j in range(len(m)):
        j_column = []
        for i in range(len(m)):
            j_column.append(m[i][j])
        columns.append(j_column)
    result.extend(columns)

    diagonals = []
    n = len(m)
    for k in range(-n, n+1):
        diagonals.append(
            [m[j+k][j] for j in range(max(-n, 0), min(n-1, n-1-k)+1)])
    for t in range(0, 2*n + 1):
        diagonals.append(
            [m[i][-i+t] for i in range(max(t+1-n, 0), min(t, n-1)+1)])

    result.extend(diagonals)
    result.append([m[0][0]])
    return result


def get_max_product(adjacent_amount, num_list):
    max_product = 0
    if adjacent_amount > len(num_list):
        return 0
    for i in range(len(num_list)-adjacent_amount):
        pick = [num_list[i+j] for j in range(adjacent_amount)]
        product = 1
        for num in pick:
            product *= num
        if max_product < product:
            max_product = product
    return max_product


def Largest_product_in_a_grid(adjacent_num):
    m = load_num()
    products = []
    all_direction_lists = build_all_direction_list(m)
    for l in all_direction_lists:
        product = get_max_product(adjacent_num, l)
        products.append(product)

    return max(products)


if __name__ == '__main__':
    r = Largest_product_in_a_grid(4)
    print(r) # r = 70600674
