def n_th_triangular_num(n):
    return int(n * (n+1)/2)


def is_multiple(num, num_list):
    for i in num_list:
        if num % i == 0:
            return True
    return False


def max_pow(num, factor):
    """ return power, quotient
    """
    remaider = num % factor
    if remaider != 0:
        return 0, num
    power = 1
    num = num / factor
    while num % factor == 0:
        power += 1
        num = num / factor
    return power, int(num)


def factor(num):
    """ num_factors prime power
    """
    primes = [2]
    test_num = 2
    num_factors = {}
    while True:
        if not is_multiple(test_num, primes) or test_num == 2:
            primes.append(test_num)
            if num % test_num == 0:
                power, num = max_pow(num, test_num)
                num_factors[test_num] = power
        test_num += 1
        if num == 1:
            break
    return num_factors


def num_of_divisors_of(tar_num):
    d = factor(tar_num)
    result = 1
    for power in d.values():
        result *= (power+1)
    return result


def Highly_divisible_triangular_number(over_num):
    n = 1
    n_th_triangular_number = n_th_triangular_num(n)
    while True:
        if num_of_divisors_of(n_th_triangular_number) > over_num:
            return n_th_triangular_number
        n = n+1
        n_th_triangular_number = n_th_triangular_num(n)



if __name__ == '__main__':
    r = Highly_divisible_triangular_number(500)
    print(r)
