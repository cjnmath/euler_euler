def load_nums():
    f = open('Problem_13.txt')
    lines = f.readlines()
    return [line.strip() for line in lines]


def cut_num(num):
    result = []
    for i in range(5):
        result.append(num[10*i:10*(i+1)])
    return result


def split_num(m):
    new_m = []
    for num in m:
        new_row = [int(i) for i in cut_num(num)]
        new_m.append(new_row)
    return new_m


def Large_sum():
    m = load_nums()
    new_m = split_num(m)
    last_sum_plus = 0
    for _ in range(4):
        last_sum = last_sum_plus
        for row in new_m:
            last_sum += row.pop()
        last_sum_plus = int(str(last_sum)[:-10])
    return sum([row[0] for row in new_m])+last_sum_plus


if __name__ == '__main__':
    r = str(Large_sum())[:10]
    print(r, len(r))
    # r = 5537376230
