def next_num(n):
    if n % 2 == 0:
        return int(n/2)
    return 3*n + 1


def chain_length_stars_from(n):
    length = 1
    next_n = next_num(n)
    while next_n != 1:
        length += 1
        next_n = next_num(next_n)
    return length+1


def Longest_Collatz_sequence(under_num):
    longest = 0
    target = 0
    for i in range(under_num):
        new_len = chain_length_stars_from(i)
        if longest < new_len:
            longest = new_len
            target = i
    return target


if __name__ == '__main__':
    r = Longest_Collatz_sequence(1000000)
    print(r)
