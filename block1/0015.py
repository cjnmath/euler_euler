def build_grid_by(n):
    m = []
    for _ in range(n+1):
        m.append([0 for _ in range(n+1)])
    return m


def Lattice_paths(n):
    m = build_grid_by(n)
    for j in range(1, n+1):
        m[0][j] = 1
    for i in range(1, n+1):
        m[i][0] = 1
    for j in range(1, n+1):
        for i in range(1, n+1):
            m[i][j] = m[i-1][j] + m[i][j-1]
    return m[-1][-1]


if __name__ == '__main__':
    r = Lattice_paths(20)
    print(r)
