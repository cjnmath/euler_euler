def digits_sum(num):
    s = 0
    str_num = str(num)
    for i in str_num:
        s += int(i)
    return s


if __name__ == '__main__':
    r = digits_sum(2**1000)
    print(r)
