def load_nums():
    f = open('Problem_18.txt')
    raw_lines = f.readlines()
    semi_lines = [line.strip() for line in raw_lines]
    lines = [line.split(' ') for line in semi_lines]
    m = []
    for line in lines:
        m.append([int(i) for i in line])
    return m


def merge_row(last_row, current_row):
    current_row[0] += last_row[0]
    current_row[-1] += current_row[-1]
    for i in range(1, len(current_row)-1):
        current_row[i] += max(last_row[i-1], last_row[i])


def Maximum_path_sum():
    m = load_nums()
    for i in range(1, len(m)):
        merge_row(m[i-1], m[i])
    return max(m[-1])


if __name__ == '__main__':
    r = Maximum_path_sum()
    print(r)  # r = 1070
