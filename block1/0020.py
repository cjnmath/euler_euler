def Factorial(n):
    if n == 1:
        return 1
    return n * Factorial(n-1)


def digits_sum(num):
    s = 0
    str_num = str(num)
    for i in str_num:
        s += int(i)
    return s


def Factorial_digit_sum(num):
    s = Factorial(num)
    return digits_sum(s)


if __name__ == '__main__':
    r = Factorial_digit_sum(100)
    print(r)
    # r = 648
