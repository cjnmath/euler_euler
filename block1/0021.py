def is_multiple(num, num_list):
    for i in num_list:
        if num % i == 0:
            return True
    return False


def max_pow(num, factor):
    """ return power, quotient
    """
    remaider = num % factor
    if remaider != 0:
        return 0, num
    power = 1
    num = num / factor
    while num % factor == 0:
        power += 1
        num = num / factor
    return power, int(num)


def factor(num):
    """ num_factors prime power
    """
    primes = [2]
    test_num = 2
    num_factors = {}
    while True:
        if not is_multiple(test_num, primes) or test_num == 2:
            primes.append(test_num)
            if num % test_num == 0:
                power, num = max_pow(num, test_num)
                num_factors[test_num] = power
        test_num += 1
        if num == 1:
            break
    return num_factors


def d(num):
    decom_fac = factor(num)
    s = 1
    for prime, power in decom_fac.items():
        t = 0
        for i in range(power+1):
            t += prime ** i
        s *= t
    return s - num


def is_amicable(a, b):
    if d(a) == b and d(b) == a:
        return True
    return False


def Amicable_numbers(under):
    result = []
    for i in range(under):
        if is_amicable(i, d(i)):
            result.append(i)
    return result


if __name__ == '__main__':
    r = Amicable_numbers(10000)
    print(r)
